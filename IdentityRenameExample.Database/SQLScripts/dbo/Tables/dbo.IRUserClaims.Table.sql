USE [IdentityRenameExample]
GO
ALTER TABLE [dbo].[IRUserClaims] DROP CONSTRAINT [FK_dbo.IRUserClaims_dbo.IRUsers_UserId]
GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [IX_UserId] ON [dbo].[IRUserClaims]
GO
/****** Object:  Table [dbo].[IRUserClaims]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP TABLE [dbo].[IRUserClaims]
GO
/****** Object:  Table [dbo].[IRUserClaims]    Script Date: 11/8/2014 1:17:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IRUserClaims](
	[UserClaimId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.IRUserClaims] PRIMARY KEY CLUSTERED 
(
	[UserClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[IRUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IRUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.IRUserClaims_dbo.IRUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[IRUsers] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IRUserClaims] CHECK CONSTRAINT [FK_dbo.IRUserClaims_dbo.IRUsers_UserId]
GO

USE [IdentityRenameExample]
GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [UserNameIndex] ON [dbo].[IRUsers]
GO
/****** Object:  Table [dbo].[IRUsers]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP TABLE [dbo].[IRUsers]
GO
/****** Object:  Table [dbo].[IRUsers]    Script Date: 11/8/2014 1:17:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IRUsers](
	[UserId] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[Discriminator] [nvarchar](256) NULL,
	[FirstName] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[DisplayName] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.IRUsers] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[IRUsers] ([UserId], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [Discriminator], [FirstName], [LastName], [DisplayName]) VALUES (N'66d0913c-1e24-4b87-98bf-6e335ba603bc', N'dave2@email.com', 0, N'AF13r8sjX6vLAbSj5oCzYHXKmdxsdaV3UcfneNIip6N1KHYxs7u6ZEOp3TJiQepy8g==', N'e8eca914-21e4-4e19-816a-eddabca979af', NULL, 0, 0, NULL, 1, 0, N'dave2@email.com', N'ApplicationUser', NULL, NULL, NULL)
INSERT [dbo].[IRUsers] ([UserId], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [Discriminator], [FirstName], [LastName], [DisplayName]) VALUES (N'aa84bcea-4077-4c49-91f2-6e30cf0c66c1', N'dave@email.com', 0, N'AJNN8Fb0ujcAN2v4wTaInay5WsAH3NDakD9umY0D/ldjnTK2aH4bPkcdmpo/cVF/MQ==', N'39253380-e7cc-4ee0-bec9-d66165a33994', NULL, 0, 0, NULL, 1, 0, N'dave@email.com', N'ApplicationUser', NULL, NULL, NULL)
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[IRUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

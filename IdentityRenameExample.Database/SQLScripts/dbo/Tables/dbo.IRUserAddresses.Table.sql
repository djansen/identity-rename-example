USE [IdentityRenameExample]
GO
ALTER TABLE [dbo].[IRUserAddresses] DROP CONSTRAINT [FK_IRUserAddresses_IRUsers]
GO
ALTER TABLE [dbo].[IRUserAddresses] DROP CONSTRAINT [FK_IRUserAddresses_IRAddresses]
GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [IX_UserId] ON [dbo].[IRUserAddresses]
GO
/****** Object:  Index [IX_AddressId]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [IX_AddressId] ON [dbo].[IRUserAddresses]
GO
/****** Object:  Table [dbo].[IRUserAddresses]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP TABLE [dbo].[IRUserAddresses]
GO
/****** Object:  Table [dbo].[IRUserAddresses]    Script Date: 11/8/2014 1:17:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IRUserAddresses](
	[UserAddressId] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[AddressId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IRUserAddresses] PRIMARY KEY CLUSTERED 
(
	[UserAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AddressId]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressId] ON [dbo].[IRUserAddresses]
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[IRUserAddresses]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IRUserAddresses]  WITH CHECK ADD  CONSTRAINT [FK_IRUserAddresses_IRAddresses] FOREIGN KEY([AddressId])
REFERENCES [dbo].[IRAddresses] ([AddressId])
GO
ALTER TABLE [dbo].[IRUserAddresses] CHECK CONSTRAINT [FK_IRUserAddresses_IRAddresses]
GO
ALTER TABLE [dbo].[IRUserAddresses]  WITH CHECK ADD  CONSTRAINT [FK_IRUserAddresses_IRUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[IRUsers] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IRUserAddresses] CHECK CONSTRAINT [FK_IRUserAddresses_IRUsers]
GO

USE [IdentityRenameExample]
GO
ALTER TABLE [dbo].[IRUserRoles] DROP CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRUsers_UserId]
GO
ALTER TABLE [dbo].[IRUserRoles] DROP CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRRoles_RoleId]
GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [IX_UserId] ON [dbo].[IRUserRoles]
GO
/****** Object:  Index [IX_RoleId]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [IX_RoleId] ON [dbo].[IRUserRoles]
GO
/****** Object:  Table [dbo].[IRUserRoles]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP TABLE [dbo].[IRUserRoles]
GO
/****** Object:  Table [dbo].[IRUserRoles]    Script Date: 11/8/2014 1:17:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IRUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.IRUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[IRUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[IRUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IRUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[IRRoles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IRUserRoles] CHECK CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRRoles_RoleId]
GO
ALTER TABLE [dbo].[IRUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[IRUsers] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IRUserRoles] CHECK CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRUsers_UserId]
GO

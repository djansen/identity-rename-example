USE [IdentityRenameExample]
GO
/****** Object:  Table [dbo].[IRAddress]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP TABLE [dbo].[IRAddresses]
GO
/****** Object:  Table [dbo].[IRAddress]    Script Date: 11/8/2014 1:17:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IRAddresses](
	[AddressId] [nvarchar](128) NOT NULL,
	[StreetLine1] [nvarchar](500) NOT NULL,
	[StreetLine2] [nvarchar](500) NULL,
	[City] [nvarchar](500) NOT NULL,
	[StateProvinceId] [nvarchar](128) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[CountryId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IRAddresses] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

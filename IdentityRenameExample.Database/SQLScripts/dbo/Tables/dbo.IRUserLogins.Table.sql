USE [IdentityRenameExample]
GO
ALTER TABLE [dbo].[IRUserLogins] DROP CONSTRAINT [FK_dbo.IRUserLogins_dbo.IRUsers_UserId]
GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP INDEX [IX_UserId] ON [dbo].[IRUserLogins]
GO
/****** Object:  Table [dbo].[IRUserLogins]    Script Date: 11/8/2014 1:17:52 PM ******/
DROP TABLE [dbo].[IRUserLogins]
GO
/****** Object:  Table [dbo].[IRUserLogins]    Script Date: 11/8/2014 1:17:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IRUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.IRUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/8/2014 1:17:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[IRUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IRUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.IRUserLogins_dbo.IRUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[IRUsers] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IRUserLogins] CHECK CONSTRAINT [FK_dbo.IRUserLogins_dbo.IRUsers_UserId]
GO

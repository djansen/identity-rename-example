using System;
using System.Collections.Generic;

namespace IdentityRenameExample.Model.Models
{
    public partial class IRUserAddress
    {
        public string UserAddressId { get; set; }
        public string UserId { get; set; }
        public string AddressId { get; set; }
        public virtual IRAddress IRAddress { get; set; }
        public virtual IRUser IRUser { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace IdentityRenameExample.Model.Models
{
    public partial class IRUserClaim
    {
        public int UserClaimId { get; set; }
        public string UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public virtual IRUser IRUser { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace IdentityRenameExample.Model.Models
{
    public partial class IRUser
    {
        public IRUser()
        {
            this.IRUserAddresses = new List<IRUserAddress>();
            this.IRUserClaims = new List<IRUserClaim>();
            this.IRUserLogins = new List<IRUserLogin>();
            this.IRRoles = new List<IRRole>();
        }

        public string UserId { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public string Discriminator { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public virtual ICollection<IRUserAddress> IRUserAddresses { get; set; }
        public virtual ICollection<IRUserClaim> IRUserClaims { get; set; }
        public virtual ICollection<IRUserLogin> IRUserLogins { get; set; }
        public virtual ICollection<IRRole> IRRoles { get; set; }
    }
}

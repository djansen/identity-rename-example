using System;
using System.Collections.Generic;

namespace IdentityRenameExample.Model.Models
{
    public partial class IRRole
    {
        public IRRole()
        {
            this.IRUsers = new List<IRUser>();
        }

        public string RoleId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<IRUser> IRUsers { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace IdentityRenameExample.Model.Models
{
    public partial class IRUserLogin
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string UserId { get; set; }
        public virtual IRUser IRUser { get; set; }
    }
}

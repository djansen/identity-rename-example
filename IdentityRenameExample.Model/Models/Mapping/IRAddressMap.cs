using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace IdentityRenameExample.Model.Models.Mapping
{
    public class IRAddressMap : EntityTypeConfiguration<IRAddress>
    {
        public IRAddressMap()
        {
            // Primary Key
            this.HasKey(t => t.AddressId);

            // Properties
            this.Property(t => t.AddressId)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.StreetLine1)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.StreetLine2)
                .HasMaxLength(500);

            this.Property(t => t.City)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.StateProvinceId)
                .HasMaxLength(128);

            this.Property(t => t.PostalCode)
                .HasMaxLength(50);

            this.Property(t => t.CountryId)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("IRAddress");
            this.Property(t => t.AddressId).HasColumnName("AddressId");
            this.Property(t => t.StreetLine1).HasColumnName("StreetLine1");
            this.Property(t => t.StreetLine2).HasColumnName("StreetLine2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.StateProvinceId).HasColumnName("StateProvinceId");
            this.Property(t => t.PostalCode).HasColumnName("PostalCode");
            this.Property(t => t.CountryId).HasColumnName("CountryId");
        }
    }
}

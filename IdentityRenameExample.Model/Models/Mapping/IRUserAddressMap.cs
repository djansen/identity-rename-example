using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace IdentityRenameExample.Model.Models.Mapping
{
    public class IRUserAddressMap : EntityTypeConfiguration<IRUserAddress>
    {
        public IRUserAddressMap()
        {
            // Primary Key
            this.HasKey(t => t.UserAddressId);

            // Properties
            this.Property(t => t.UserAddressId)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.UserId)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.AddressId)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("IRUserAddresses");
            this.Property(t => t.UserAddressId).HasColumnName("UserAddressId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.AddressId).HasColumnName("AddressId");

            // Relationships
            this.HasRequired(t => t.IRAddress)
                .WithMany(t => t.IRUserAddresses)
                .HasForeignKey(d => d.AddressId);
            this.HasRequired(t => t.IRUser)
                .WithMany(t => t.IRUserAddresses)
                .HasForeignKey(d => d.UserId);

        }
    }
}

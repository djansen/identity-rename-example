using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace IdentityRenameExample.Model.Models.Mapping
{
    public class IRUserClaimMap : EntityTypeConfiguration<IRUserClaim>
    {
        public IRUserClaimMap()
        {
            // Primary Key
            this.HasKey(t => t.UserClaimId);

            // Properties
            this.Property(t => t.UserId)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("IRUserClaims");
            this.Property(t => t.UserClaimId).HasColumnName("UserClaimId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.ClaimType).HasColumnName("ClaimType");
            this.Property(t => t.ClaimValue).HasColumnName("ClaimValue");

            // Relationships
            this.HasRequired(t => t.IRUser)
                .WithMany(t => t.IRUserClaims)
                .HasForeignKey(d => d.UserId);

        }
    }
}

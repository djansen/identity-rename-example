using System;
using System.Collections.Generic;

namespace IdentityRenameExample.Model.Models
{
    public partial class IRAddress
    {
        public IRAddress()
        {
            this.IRUserAddresses = new List<IRUserAddress>();
        }

        public string AddressId { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string StateProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string CountryId { get; set; }
        public virtual ICollection<IRUserAddress> IRUserAddresses { get; set; }
    }
}

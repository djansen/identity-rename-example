﻿IF OBJECT_ID('IRAddress') IS NOT NULL
BEGIN
	DROP TABLE IRAddress;
END;
GO


IF OBJECT_ID('IRUserRoles') IS NOT NULL
BEGIN
	DROP TABLE IRUserRoles;
END;
GO

IF OBJECT_ID('IRUserLogins') IS NOT NULL
BEGIN
	DROP TABLE IRUserLogins;
END;
GO

IF OBJECT_ID('IRUserClaims') IS NOT NULL
BEGIN
	DROP TABLE IRUserClaims;
END;
GO

IF OBJECT_ID('IRRoles') IS NOT NULL
BEGIN
	DROP TABLE IRRoles;
END;
GO

IF OBJECT_ID('IRUsers') IS NOT NULL
BEGIN
	DROP TABLE IRUsers;
END;
GO


CREATE TABLE [dbo].[IRAddress](
	[AddressId] [nvarchar](128) NOT NULL,
	[StreetLine1] [nvarchar](500) NOT NULL,
	[StreetLine2] [nvarchar](500) NULL,
	[City] [nvarchar](500) NOT NULL,
	[StateProvinceId] [nvarchar](128) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[CountryId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_IRAddress] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[IRUsers] (
	[UserId]               NVARCHAR (128) NOT NULL,
	[Email]                NVARCHAR (256) NULL,
	[EmailConfirmed]       BIT            NOT NULL,
	[PasswordHash]         NVARCHAR (MAX) NULL,
	[SecurityStamp]        NVARCHAR (MAX) NULL,
	[PhoneNumber]          NVARCHAR (MAX) NULL,
	[PhoneNumberConfirmed] BIT            NOT NULL,
	[TwoFactorEnabled]     BIT            NOT NULL,
	[LockoutEndDateUtc]    DATETIME       NULL,
	[LockoutEnabled]       BIT            NOT NULL,
	[AccessFailedCount]    INT            NOT NULL,
	[UserName]             NVARCHAR (256) NOT NULL,
	[Discriminator]        NVARCHAR (256) NULL,
	[FirstName]		       NVARCHAR (256) NULL,
	[LastName]		       NVARCHAR (256) NULL,
	[DisplayName]	       NVARCHAR (256) NULL
	CONSTRAINT [PK_dbo.IRUsers] PRIMARY KEY CLUSTERED ([UserId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
	ON [dbo].[IRUsers]([UserName] ASC);

GO 
CREATE TABLE [dbo].[IRRoles] (
	[RoleId]   NVARCHAR (128) NOT NULL,
	[Name] NVARCHAR (256) NOT NULL,
	CONSTRAINT [PK_dbo.IRRoles] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
	ON [dbo].[IRRoles]([Name] ASC);

GO

CREATE TABLE [dbo].[IRUserClaims] (
	[UserClaimId]         INT            IDENTITY (1, 1) NOT NULL,
	[UserId]     NVARCHAR (128) NOT NULL,
	[ClaimType]  NVARCHAR (MAX) NULL,
	[ClaimValue] NVARCHAR (MAX) NULL,
	CONSTRAINT [PK_dbo.IRUserClaims] PRIMARY KEY CLUSTERED ([UserClaimId] ASC),
	CONSTRAINT [FK_dbo.IRUserClaims_dbo.IRUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[IRUsers] ([UserId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
	ON [dbo].[IRUserClaims]([UserId] ASC);

GO

CREATE TABLE [dbo].[IRUserLogins] (
	[LoginProvider] NVARCHAR (128) NOT NULL,
	[ProviderKey]   NVARCHAR (128) NOT NULL,
	[UserId]        NVARCHAR (128) NOT NULL,
	CONSTRAINT [PK_dbo.IRUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
	CONSTRAINT [FK_dbo.IRUserLogins_dbo.IRUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[IRUsers] ([UserId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
	ON [dbo].[IRUserLogins]([UserId] ASC);

GO
CREATE TABLE [dbo].[IRUserRoles] (
	[UserId] NVARCHAR (128) NOT NULL,
	[RoleId] NVARCHAR (128) NOT NULL,
	CONSTRAINT [PK_dbo.IRUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
	CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[IRRoles] ([RoleId]) ON DELETE CASCADE,
	CONSTRAINT [FK_dbo.IRUserRoles_dbo.IRUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[IRUsers] ([UserId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
	ON [dbo].[IRUserRoles]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
	ON [dbo].[IRUserRoles]([RoleId] ASC);


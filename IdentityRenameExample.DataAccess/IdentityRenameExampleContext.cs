using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using IdentityRenameExample.Model.Models.Mapping;

namespace IdentityRenameExample.Model.Models
{
    public partial class IdentityRenameExampleContext : DbContext
    {
        static IdentityRenameExampleContext()
        {
            Database.SetInitializer<IdentityRenameExampleContext>(null);
        }

        public IdentityRenameExampleContext()
            : base("Name=IdentityRenameExampleContext")
        {
        }

        public DbSet<IRAddress> IRAddresses { get; set; }
        public DbSet<IRRole> IRRoles { get; set; }
        public DbSet<IRUserAddress> IRUserAddresses { get; set; }
        public DbSet<IRUserClaim> IRUserClaims { get; set; }
        public DbSet<IRUserLogin> IRUserLogins { get; set; }
        public DbSet<IRUser> IRUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new IRAddressMap());
            modelBuilder.Configurations.Add(new IRRoleMap());
            modelBuilder.Configurations.Add(new IRUserAddressMap());
            modelBuilder.Configurations.Add(new IRUserClaimMap());
            modelBuilder.Configurations.Add(new IRUserLoginMap());
            modelBuilder.Configurations.Add(new IRUserMap());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using IdentityRenameExample.Model;
using IdentityRenameExample.Model.Models;
using System.Data.Entity;
using IdentityRenameExample.Model.Models.Mapping;
using IdentityRenameExample.Model.Identity;

namespace IdentityRenameExample.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }



        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("IRUsers").Property(p => p.Id).HasColumnName("UserId");

            var user = modelBuilder.Entity<IdentityUser>().ToTable("IRUsers");
            user.Property(p => p.Id).HasColumnName("UserId");
            user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            user.HasMany(u => u.Claims).WithRequired().HasForeignKey(uc => uc.UserId);
            user.HasMany(u => u.Logins).WithRequired().HasForeignKey(ul => ul.UserId);
            user.Property(u => u.UserName).IsRequired();

            modelBuilder.Entity<IdentityUserRole>()
                .HasKey(r => new { r.UserId, r.RoleId })
                .ToTable("IRUserRoles");

            modelBuilder.Entity<IdentityUserLogin>()
                .HasKey(l => new { l.UserId, l.LoginProvider, l.ProviderKey })
                .ToTable("IRUserLogins");

            modelBuilder.Entity<IdentityUserClaim>().ToTable("IRUserClaims").Property(p => p.Id).HasColumnName("UserClaimId");

            var role = modelBuilder.Entity<IdentityRole>()
                .ToTable("IRRoles");
            role.Property(r => r.Name).IsRequired();
            role.HasMany(r => r.Users).WithRequired().HasForeignKey(ur => ur.RoleId);
            role.Property(p => p.Id).HasColumnName("RoleId");
        }
    }
}

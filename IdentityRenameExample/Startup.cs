﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IdentityRenameExample.Startup))]
namespace IdentityRenameExample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
